﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorsemanSpawner : MonoBehaviour
{
    public static HorsemanSpawner instance;

    public GameObject horsemanPrefab;
    public float spawnCount; // 15
    public float minRandomSpawnTime, maxRandomSpawnTime; //min 2 max 4

    private int _spawnCount;
    private float timer;

    private void Start()
    {
        if (instance == null) instance = this;
        timer = 4f;
        _spawnCount = 0;

        int currentLevel = PlayerPrefs.GetInt("CurrentLevel", 1);

        if (currentLevel < 5) { 
        spawnCount = currentLevel + Random.Range(1 , 5);
        }
        else if (currentLevel > 10)
        {
            spawnCount = currentLevel + Random.Range(5, 10);
        } 



    }
    
    private void Update()
    {
        Spawner();
    }

    private void Spawner()
    {
        timer += Time.deltaTime;
        float t = Random.Range(minRandomSpawnTime, maxRandomSpawnTime);
        if(t <= timer && spawnCount > _spawnCount)
        {
            Quaternion horsemanRotation = Quaternion.Euler(0, 180, 0);
            Instantiate(horsemanPrefab, spawnPosition(), horsemanRotation);
            timer = 0f;
            _spawnCount++;
        }
    }

    private Vector3 spawnPosition()
    {
        int k = Random.Range(1, 3);
        Vector3 spawnerPosition = transform.position + Vector3.up * .3f;
        Vector3 spawnOffset;
        if (k == 1) spawnOffset = Vector3.left * Random.Range(0.6f, 1.0f);
        else spawnOffset = Vector3.right * Random.Range(0.6f, 1.0f);
        Vector3 desiredSpawnPosition = spawnerPosition + spawnOffset;
        return desiredSpawnPosition;
    }
}
