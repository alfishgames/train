﻿using UnityEngine;

public class Rock : MonoBehaviour
{
    public GameObject target;

    void Update()
    {
        if (gameObject.CompareTag("Rock_Throw"))
        {
            transform.position = Vector3.Slerp(transform.position, target.transform.position, Time.deltaTime * 10);
        }
        
        if (gameObject.CompareTag("Enemy_Bullet"))
        {
            transform.position = Vector3.Slerp(transform.position, target.transform.position + Vector3.back * 1.6f + Vector3.up * .5f, Time.deltaTime * 4);
        }
    }
}
