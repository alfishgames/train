﻿using UnityEngine;

public class Animations : MonoBehaviour
{
    public Animator anim;


    private void Awake()
    {
        anim = gameObject.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (Player.instance.state == Player.State.idle)
        {
            anim.SetBool("Move", false);
        }

        if (Player.instance.state == Player.State.moving)
        {
            anim.SetBool("Move", true);
        }

        if (Player.instance.animPick)
        {
            Player.instance.animPick = false;
            anim.SetTrigger("Pick");
        }


        if (Player.instance.animThrow)
        {
            Player.instance.animThrow = false;
            anim.SetTrigger("Throw");
        }


    }
}
