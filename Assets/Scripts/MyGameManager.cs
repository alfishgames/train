﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour
{
    public static MyGameManager instance;
    public GameObject successPanel,winParticle;
    public GameObject failedPanel;

    [HideInInspector]
    public bool succes, failed;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        //TinySauce.OnGameStarted(PlayerPrefs.GetInt("CurrentLevel", 1).ToString());
        Debug.Log(PlayerPrefs.GetInt("CurrentLevel", 1));
    }

    public void Success()
    {
        winParticle.SetActive(true);
        succes = true;
        successPanel.SetActive(true);        
    }

    public void Failed()
    {
        failed = true;
        failedPanel.SetActive(true);
    }

    public void NextLevelButton()
    {
        //TinySauce.OnGameFinished(PlayerPrefs.GetInt("CurrentLevel", 1).ToString(), true, 1);
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel", 1) + 1);
        LoadAsync.GlobalAsync.TriggerManually();
    }


    public void RestartLevelButton()
    {
        LoadAsync.GlobalAsync.TriggerManually();
    }

}
