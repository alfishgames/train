﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Database", menuName = "Database")]
public class Database : ScriptableObject
{
    public float trainVelocity;
    public int trainHealth;
    public int playerRockCount;
    public float horsemanSpawnCount;
    public float horsemanSpawnTimeMax;
    public float horsemanSpawnTimeMin;
    public float rockSpeed;

    private void Awake()
    {
        
    }
}
