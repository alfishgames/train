﻿using System;
using UnityEngine;

public class Horseman : MonoBehaviour
{
    public Database database;
    public enum State { Drive, Hit, Attack }
    public State state;
    public GameObject myHand, rock;
    public float throwRate;

    public GameObject stickman;
    private GameObject train;
    private Rigidbody myRb;
    private BoxCollider myCol;
    private Animator enemyAnim;
    private bool hit, attack,oneTimeExecute;

    private void Awake()
    {
        enemyAnim = stickman.GetComponent<Animator>();
        state = State.Drive;
        train = GameObject.Find("Train");
        myRb = GetComponent<Rigidbody>();
        myCol = GetComponent<BoxCollider>();
        oneTimeExecute = true;

    }

    private void Update()
    {
        ToAttack();
        States();
    }
    private void States()
    {
        if (state == State.Drive)
        {
            DriveToTrain();
        }

        if (state == State.Hit)
        {
            GetHit();
            if (!Player.instance.hitHorseman.Contains(gameObject)) Player.instance.hitHorseman.Add(gameObject);
        }

        if (state == State.Attack)
        {
            Attack();
        }
    }
    
    private void GetHit()
    {
        #region Force
        transform.position += Vector3.forward * Time.deltaTime * 7;
        Vector3 explosionForceR = Vector3.up * 1.2f + Vector3.right + Vector3.forward * 2;
        Vector3 explosionForceL = Vector3.up * 1.2f + Vector3.left + Vector3.forward * 2;
        Vector3 direction = transform.position - train.transform.position;
        myRb.AddTorque(Vector3.right * UnityEngine.Random.Range(-150, 150) * Time.deltaTime);
        myRb.AddTorque(Vector3.up * UnityEngine.Random.Range(-150, 150) * Time.deltaTime);
        myRb.AddTorque(Vector3.forward * UnityEngine.Random.Range(-150, 150) * Time.deltaTime);
        myRb.useGravity = true;
        myCol.isTrigger = false;

        #endregion

        if (direction.x > 0 && !hit)
        {
            hit = true;
            myRb.AddForce(explosionForceR * 30f, ForceMode.VelocityChange);

        }

        else if (direction.x < 0 && !hit)
        {
            hit = true;
            myRb.AddForce(explosionForceL * 30f, ForceMode.VelocityChange);

        }

        if (oneTimeExecute)
        {
        
            GetComponentInChildren<ExplosionManager>().Explode();
            oneTimeExecute = false;
        }



    }
    private void DriveToTrain()
    {
        transform.position += Vector3.back * Time.deltaTime * 2.1f;
    }

    private void Attack()
    {
        if (!attack && !MyGameManager.instance.failed && !MyGameManager.instance.succes)
        {
            attack = true;
            GameObject tRock = Instantiate(rock, myHand.transform.position, Quaternion.identity);
            tRock.GetComponent<Rock>().target = train;
            tRock.tag = "Enemy_Bullet";
            enemyAnim.SetTrigger("Throw");
            Invoke(nameof(ResetAttack), throwRate);
        }
    }

    private void ResetAttack()
    {
        attack = false;
    }

    private void ToAttack()
    {
        Vector3 distance = transform.position - train.transform.position;
        if (distance.z <= -0.6f && state != State.Hit)
        {
            state = State.Attack;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rock_Throw"))
        {
            state = State.Hit;
            Player.instance.horsemanList.Remove(gameObject);
            Destroy(other.gameObject);
        }
    }



}
