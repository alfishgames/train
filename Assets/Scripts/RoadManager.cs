﻿using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour
{
    public static RoadManager instance;
    public Database database;
    public GameObject road;
    public float forwardSpeed;

    private List<GameObject> roadList = new List<GameObject>();
    private float timer;
    private float spawnTime;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        spawnTime = 32.2f / forwardSpeed;
        roadList.Add(Instantiate(road));
        roadList.Add(Instantiate(road, roadList[0].transform.position + -Vector3.forward * 32.352f, Quaternion.identity));
    }

    private void Update()
    {
        timer += Time.deltaTime;

        for (int i = 0; i < roadList.Count; i++)
        {
            roadList[i].transform.position += forwardSpeed * Vector3.forward * Time.deltaTime;
        }

        if (timer >= spawnTime)
        {
            roadList.Add(Instantiate(road, roadList[1].transform.position + -Vector3.forward * 32.352f, Quaternion.identity));
            Destroy(roadList[0]);
            roadList.RemoveAt(0);
            timer = 0f;
        }
    }
}
