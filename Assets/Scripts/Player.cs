﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance;
    public Joystick joystick;
    public GameObject rock;
    public GameObject rockPrefab;
    public GameObject target;
    public int rockCount;

    public enum State { idle, moving }
    [HideInInspector]
    public State state;

    //[HideInInspector]
    public bool hasRock, animPick, animThrow, threw, resetAttack, inRange;

    public List<GameObject> horsemanList = new List<GameObject>();
    public List<GameObject> hitHorseman = new List<GameObject>();
    public LayerMask horsemanLayer;

    private Horseman[] horseman;
    private float verticalMove, horizontalMove, attackRange;
    private GameObject tRock, inGameMenuUI;
    private Transform hand;
    private SphereCollider myDetectCol;
    private BoxCollider rocksCollider;
    private Vector3 movement, lastposition;

    private void Awake()
    {
        if (instance == null) instance = this;
        hand = GameObject.FindGameObjectWithTag("Player_Right_Hand").transform;
        inGameMenuUI = GameObject.Find("/Canvas/InGame_UI");
        ColliderSettings();
    }

    private void Update()
    {
        SetJoystickVariables();
        StateController();
        Movement();
        ThrowRock();
        Horsemans();
    }


    private void StateController()
    {
        if (movement != Vector3.zero) state = State.moving;
        else state = State.idle;
        if(hitHorseman.Count == HorsemanSpawner.instance.spawnCount && !MyGameManager.instance.succes)
        {
            MyGameManager.instance.Success();
        }
    }

    private void SetJoystickVariables()
    {
        verticalMove = joystick.Vertical;
        horizontalMove = joystick.Horizontal;
        movement = new Vector3(horizontalMove, 0, verticalMove);
    }

    private void Horsemans()
    {
        horseman = new Horseman[horsemanList.Count];
        for (int i = 0; i < horsemanList.Count; i++)
        {
            horseman[i] = horsemanList[i].gameObject.GetComponent<Horseman>();
        }
    }

    private void Movement()
    {
        transform.position += movement * Time.deltaTime;
        Vector3 direction = movement.normalized;

        if (direction != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 15);
            lastposition = direction;
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lastposition), Time.deltaTime * 15);
        }
    }

    public void ThrowRock()
    {
        inRange = Physics.CheckSphere(transform.position, attackRange, horsemanLayer);
        Text rockText = inGameMenuUI.GetComponentInChildren<Text>();
        rockText.text = rockCount.ToString();

        if (rockCount == 0)
        {
            hasRock = false;
            rock.SetActive(false);
            rockText.text = "You dont have rock";
        }

        else if (rockCount > 0 && threw && inRange)
        {
            animThrow = true;
            threw = false;
            rockCount--;
            Invoke(nameof(SpawnRock), 0.3f);
        }
    }

     private void SpawnRock()
    {
        tRock = Instantiate(rockPrefab, hand.position, Quaternion.identity);
        target = horsemanList[0].gameObject;
        tRock.GetComponent<Rock>().target = target;
        tRock.tag = "Rock_Throw";
    }

    private void ColliderSettings()
    {
        myDetectCol = gameObject.GetComponentInChildren<SphereCollider>();
        rocksCollider = GameObject.Find("Rocks").GetComponent<BoxCollider>();
        Physics.IgnoreCollision(myDetectCol, rocksCollider);
        attackRange = myDetectCol.radius;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rocks"))
        {
            if (!hasRock)
            {
                rock.SetActive(true);
                animPick = true;
                hasRock = true;
                rockCount = 2;
            }
        }

        if (other.gameObject.CompareTag("Horseman"))
        {
            if (horsemanList.Contains(other.gameObject))
            {
                Debug.Log("This " + other.gameObject.name + " in the list.");
            }
            else
            {
                horsemanList.Add(other.gameObject);
                threw = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
