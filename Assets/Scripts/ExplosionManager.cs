﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        setRigidbodyState(true);
        setColliderState(false,false);

    }

    void setRigidbodyState(bool state)
    {

        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
        

        foreach (Rigidbody rigidbody in rigidbodies)
        { 

            rigidbody.isKinematic = state;

         
            rigidbody.AddExplosionForce(UnityEngine.Random.Range(50, 100), transform.position, UnityEngine.Random.Range(100, 150), UnityEngine.Random.Range(100, 150), ForceMode.Force);


        }

       // GetComponent<Rigidbody>().isKinematic = !state;

    }


    void setColliderState(bool state, bool isTrigger)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = state;
            if (isTrigger)
            {
                collider.isTrigger = isTrigger;
            }

        }

        try
        {
            GetComponent<Collider>().enabled = !state;
        }
        catch (Exception ex) { }

    }


    public void Explode()
    {

        gameObject.transform.parent = null;
        setRigidbodyState(false);
        setColliderState(true,false);
        GetComponent<Animator>().enabled = false;
        Invoke(nameof(InvokedTrigger), 1f);

    }



    private void InvokedTrigger()
    {
        setColliderState(false, false);
        Debug.Log("Invoked Trigger");
    }




}
