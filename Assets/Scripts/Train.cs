﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Train : MonoBehaviour
{
    public static Train instance;

    public int maxHealth;

    private Text healthText;
    private int health;


    private void Awake()
    {
        if (instance == null) instance = this;
        healthText = GameObject.Find("/Canvas/InGame_UI/HealthText").GetComponent<Text>();
        health = maxHealth;
    }

    private void Update()
    {
        if (health != 0) healthText.text = "Train Health : " + health.ToString();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetDamage(1);
        }
    }

    private void GetDamage(int damage)
    {
        health -= damage;

        if(health <= 0)
        {
            health = 0;
            healthText.text = "You Failed";
            MissionFailed();
        }
    }


    private void MissionFailed()
    {
        MyGameManager.instance.Failed();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy_Bullet"))
        {
            GetDamage(1);
            Destroy(other.gameObject);
        }
    }
}
